FROM python:alpine3.7
EXPOSE 8000
COPY server.py .
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD [ "python", "./server.py" ]